# Agile Books and stuff

Curated list of books and videos which inspire

* **Crucial conversations** https://amzn.to/2nNGQzz
Forget feedback sandwich, feedback wraps and other fake, b***t approaches to communicating with people about sensitive topics. This book describes approaches which helped me resolve dozens of problems without damaging relationships I had or wanted to build with other people. Tool that can be used in both personal and professional life. 
Suggested follow up topic — non violent communication.

* **GameStorming** https://amzn.to/2vRBHeB
Book which describes set of tools and games which could be used in several areas of facilitation. Helps to understand group dynamics, design workshops with good flow between its elements and provides practical advice on how to prepare for running them. 
Suggested follow up book — Training from the back of the room.

* **Leadership and self deception**, https://amzn.to/2MRcvuX
Contains surprising truth about how our brains are protecting us from feeling bad about ourselves and uncovers internal narration patterns we constantly go through in our minds. Most likely you won’t feel good about yourself reading this book, but it has a power of removing the blind and enabling new levels of relationships, team work and leadership. I found myself re-reading it every couple of months as the brain never stops…

* **The Coach’s casebook**, https://amzn.to/2MUooQW
One day a person will come to you with a problem which cannot be solved by teaching or mentoring. This book can help you understand how to utilise purely your brain power to help others find answers and motivation to change within themselves. Describes a set of personal traits and techniques to consider in each situation. Follow up read — The Coaching Manual.

* **Example mapping**, https://www.youtube.com/watch?v=VwvrGfWmG_U
Simple technique for focused, organised approach to 3 Amigos and identifying usage scenarios and test cases. If you want a wider view on how to build quality into your product, create shared understanding and identify unknown unknowns, look up Behaviour Driven Development (BDD) instead.

* **Product Ownership in a nutshell**, https://www.youtube.com/watch?v=502ILHjX9EE

* **Story Mapping** 
Book — https://amzn.to/2PiD2Dc  —  Video — https://www.youtube.com/watch?v=AorAgSrHjKM
A must-have technique in every Product Manager’s toolbox. Regardless of whether you’re junior or experienced, empowered or working in feature factory, this tool can help you have focused, richer and more valuable conversations with customers, stakeholders and teams.

* **David Evans on User stories**, https://www.youtube.com/watch?v=fsIrLr-_rCc
A bit deeper dive into user stories. Plenty of people found it useful.

* **Impact mapping**, https://amzn.to/2L1wdlT 
If Story mapping is useful then Impact mapping is game changing. Amongst other things this is conversational technique which allows to discover what really matters to customers and stakeholders, how to capture it by means of measurable behaviour changes and how to link it to product goals.

* **Scrum Princess**, https://amzn.to/2PlUZkm
Highly recommended by community, difficult to put down.

* **Scrum Mastery**, https://amzn.to/2PjkHGf
It helped myself, it helped my friends. Excellent book which provides guidance on how to improve as Scrum Master.

* **Principles of the Kanban method**, 
http://www.djaa.com/principles-kanban-method-0

* **Cumulative flow diagram explained**,  https://www.youtube.com/watch?v=eo2uv8avEsU
Surprisingly many people don’t know how to read CFD charts. 2 minute watch.

* **Personas — patterns and anti-patterns**,  https://www.youtube.com/watch?v=CU0fQgPYbbc
Assuming you know the basic concept behind personas, the video will take you through what’s good, bad and ugly. Nicely explained.

* **Design Sprints**, https://amzn.to/2nLrRGH

* **Feature injection, Chris Matts**,  https://www.youtube.com/watch?v=hZdYR1fb4Gk
Simplified and clean explanation of product/feature creation process. One of the best I saw in a while, I wish all Product Managers (POs) I work with have seen this.

* **: How to change things when change is hard**, https://amzn.to/2nLpd3J
The book shares new perspective on change process including techniques for navigating change, making change easier and getting unstuck. Helped me shape my skills as a change agent. Followup read — change model(s).

* **Simplistic explanation of business value and metrics driven approach**, 
https://theitriskmanager.wordpress.com/2017/08/20/why-business-cases-are-toxic/

* **On MVP (Minimum Viable Product)**,  https://www.linkedin.com/pulse/testing-meaningful-hypothesis-core-any-mvp-mike-dixon

* **How to Measure Anything**, https://amzn.to/2Mqgcf1
Once you get to the point when you can follow metrics driven approach, two biggest problems are: How to measure X? What should be measured, actually? This book can help.

* **Adapt: Why Success Always starts with failure**, https://amzn.to/2vXHE9G
It may be soft introduction to complex problem areas and about how to treat failure as a beneficial event.

* **Cynefin**, 
Video: https://www.youtube.com/watch?v=HVx_jIBqumc&feature=youtu.be&t=1877 
Video: https://youtu.be/-F4enP8oBFM?t=43
Must-understand concept. Every day we’re being bombarded with dozens of ideas, case studies, methods and ‘best practices’. Cynefin can help you understand what type of approaches work for a given context and organise the knowledge you accumulate.

* **Wardley Strategy Maps** 
Book online: https://medium.com/wardleymaps/on-being-lost-2ef5f05eb1ec 
Video: https://www.youtube.com/watch?v=ek0aWj_rWYs
Recommended for C-level executives and senior managers who want to better understand context they’re in, their competition, future market movements and create better strategies as a result.

* **Inspired**, https://amzn.to/2Pp6RSB
Everyone who’s familiar with Cynefin, knows creating products customers love is a complex, unpredictable domain. This book helps you navigate in such domain and win the hearts of your customers.

* **Drive, Dan Pink**, Book — https://amzn.to/2PiADbJ
Simplified, but hugely useful introduction to people’s motivation. Search his TED talk if you’re not convinced (yet) to read the book.

* **Building successful communities of practice**, https://amzn.to/2w6CS98
Do you want to empower people with information and autonomy, but don’t know how to help them organise in order to tackle big topics, such as professional development or system architecture? This book can help you create your first communities.

* **Moving motivators**, 
https://management30.com/practice/moving-motivators/ 
MM video: https://www.youtube.com/watch?v=XeS1uqqKQvs
Try this technique, if you want to understand what motivates you or your team, or how their motivation would change in a response to a given change initiative. Simple, but it’s a great conversation starter.

* **Delegation poker**,
https://management30.com/product/delegation-poker/ 
DP video: https://www.youtube.com/watch?v=VZF-G7MCSG4
Helpful technique in situations where decision making responsibilities are unclear or empowerment needs to be discussed and visualised.

* **Management 3.0**,
Managing for happiness: https://amzn.to/2ME0oVk
Management 3.0: https://amzn.to/2MCyhps
More things like delegation poker and moving motivators, from tools and techniques to guidelines for managers and organisations.

* **Enterpreneurship, in enterprise context**,  https://www.youtube.com/watch?v=MBKD0vTEty4

* **Creating learning culture in Paypal**,  https://vimeo.com/91537336

* **Experiments at Airbnb**, https://www.youtube.com/watch?v=lVTIcf6IhY4
A/B testing is just a tip of an iceberg. There is much more underneath the surface.

* **The Principles of Product Development flow**, https://amzn.to/2BjTnEH
Understanding the differences between managing factory and managing design factory is essential to succeeding in software development, yet so many managers in tech don’t know the difference and make mistakes repeated by thousands of others. The book is condensed, difficult read, but worth invested time.

* **Accelerate: The Science of Lean software and Devops**, https://amzn.to/2w8Kt7h
If you know concepts of continuous delivery, architecture, thing or two about building products, lean management and culture, but still looking for recipe for how those concepts should be combined together, this book is what you’re looking for. It’s supported by research, so if you’re looking for silver bullets, this is as close as you will get.

* **Henrik Kniberg on Alignment at Scale**, 
https://www.youtube.com/watch?v=5aQEbBGi_BM
Interesting watch even if you don’t develop products at scale. There are some references to how SAFe was customised (LEGO), for those who need to work in SAFe. There are many references to how Spotify aligns at scale, for those who copy Spotify or want to create similar culture themselves.

* **OKRs, How Google sets goals**, 
Book — https://amzn.to/2whN0Mn
Video — https://www.youtube.com/watch?v=mJB83EZtAjc

* **Agile in HR**, https://www.youtube.com/watch?v=uNUYJnt8h58

* **Build a Workplace People Love**, https://vimeo.com/144370269